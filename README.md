# ucxx

A lightweight modern C++ library for embedded systems

## Docker

Run any commands using the `docker-run.sh` script. Without providing any
arguments, it will run a Docker container in an interactive mode. If you provide
arguments to the script, these arguments will be executed inside a Docker
container.

## Build

Run the `docker-run.sh` script:

```
./docker-run.sh
```

Create a build directory:

```
mkdir -p build
```

Change the current location to the build directory:

```
cd ./build
```

Run the `CMake` project configuration step:

```
cmake ..
```

Build the project:

```
cmake --build . --target all
```

Alternatively you can run all these steps through the `docker-run.sh` script:

```
./docker-run.sh "cmake -S . -B ./build && cmake --build ./build --target all"
```

## Tests

To build and run all tests:

```
./docker-run.sh cmake -S . -B ./build
./docker-run.sh cmake --build ./build --target test
```

To build and run all tests with an enabled code coverage:

```
./docker-run.sh cmake -DCMAKE_BUILD_TYPE=Debug -DUCXX_COVERAGE=ON -S . -B ./build
./docker-run.sh cmake --build ./build --target test
```
