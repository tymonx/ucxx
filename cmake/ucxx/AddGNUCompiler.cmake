# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (NOT CMAKE_CXX_COMPILER_ID MATCHES GNU)
    return()
endif()

if (NOT TARGET ucxx-compiler-options)
    add_library(ucxx-compiler-options INTERFACE)
endif()

set(link_options "")
set(compile_options "")
set(compile_definitions "")

list(APPEND compile_options -std=c++17)

if (NOT UCXX_EXCEPTIONS)
    list(APPEND compile_options -fno-exceptions)
endif()

if (NOT UCXX_RTTI)
    list(APPEND compile_options -fno-rtti)
endif()

if (UCXX_LTO)
    list(APPEND compile_options -flto)
endif()

if (UCXX_WARNINGS_INTO_ERRORS)
    list(APPEND compile_options -Werror)
endif()

if (UCXX_COVERAGE)
    list(APPEND link_options
        --coverage
    )

    list(APPEND compile_options
        -fprofile-arcs
        -ftest-coverage
        -fno-inline
        -fno-inline-small-functions
        -fno-default-inline
    )
endif()

if (CMAKE_BUILD_TYPE MATCHES "Release" OR NOT CMAKE_BUILD_TYPE)
    list(APPEND compile_options
        -O3
        -s
        -fdata-sections
        -ffunction-sections
    )

    list(APPEND compile_definitions NDEBUG)

    if (NOT CMAKE_SYSTEM_NAME MATCHES "Generic")
        list(APPEND link_options -fstack-protector-all)

        if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.9)
            list(APPEND link_options -fstack-protector-strong)
        endif()
    endif()

    list(APPEND link_options
        -Wl,--gc-sections
        -Wl,--strip-all
        -Wl,-z,noexecstack
        -Wl,-z,relro
        -Wl,-z,now
    )
elseif (CMAKE_BUILD_TYPE MATCHES "MinSizeRel")
    list(APPEND compile_options
        -Os
        -fdata-sections
        -ffunction-sections
    )

    list(APPEND compile_definitions NDEBUG)

    list(APPEND link_options -Wl,--gc-sections)
elseif (CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
    list(APPEND compile_options
        -O3
        -g3
        -ggdb
        -fdata-sections
        -ffunction-sections
    )

    if (NOT CMAKE_SYSTEM_NAME MATCHES "Generic")
        list(APPEND link_options -fstack-protector-all)

        if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.9)
            list(APPEND link_options -fstack-protector-strong)
        endif()
    endif()

    list(APPEND link_options
        -Wl,--gc-sections
        -Wl,-z,noexecstack
        -Wl,-z,relro
        -Wl,-z,now
    )
elseif (CMAKE_BUILD_TYPE MATCHES "Debug")
    list(APPEND compile_options
        -O0
        -g3
        -ggdb
    )
endif()

list(APPEND compile_options
    -fstrict-aliasing
    -pedantic
    -Wall
    -Wcast-qual
    -Wcomments
    -Wconversion
    -Wctor-dtor-privacy
    -Wdisabled-optimization
    -Wendif-labels
    -Wenum-compare
    -Wfloat-equal
    -Wformat=2
    -Wformat-nonliteral
    -Winit-self
    -Winvalid-pch
    -Wlogical-op
    -Wmissing-declarations
    -Wmissing-include-dirs
    -Wno-long-long
    -Wnon-virtual-dtor
    -Wold-style-cast
    -Woverloaded-virtual
    -Wpacked
    -Wparentheses
    -Wpointer-arith
    -Wredundant-decls
    -Wshadow
    -Wsign-conversion
    -Wsign-promo
    -Wstack-protector
    -Wstrict-null-sentinel
    -Wstrict-overflow=2
    -Wsuggest-attribute=noreturn
    -Wswitch-default
    -Wswitch-enum
    -Wundef
    -Wuninitialized
    -Wunknown-pragmas
    -Wunused
    -Wunused-function
    -Wwrite-strings
)

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 6.0)
    list(APPEND compile_options
        -Wduplicated-cond
        -Whsa
        -Wignored-attributes
        -Wmisleading-indentation
        -Wnull-dereference
        -Wplacement-new=2
        -Wshift-negative-value
        -Wshift-overflow=2
        -Wvirtual-inheritance
    )
endif()

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 5.0)
    list(APPEND compile_options
        -Wdouble-promotion
        -Wsized-deallocation
        -Wsuggest-final-methods
        -Wsuggest-final-types
        -Wsuggest-override
        -Wtrampolines
        -Wvector-operation-performance
        -Wzero-as-null-pointer-constant
    )
endif()

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.9)
    list(APPEND compile_options
        -Wconditionally-supported
        -Wdate-time
        -Weffc++
        -Wextra
        -Winline
        -Wopenmp-simd
    )
endif()

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    list(APPEND compile_options
        -Wpedantic
        -Wsuggest-attribute=format
    )
endif()

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.6)
    list(APPEND compile_options
        -Wnoexcept
    )
endif()

if (link_options)
    target_link_options(ucxx-compiler-options INTERFACE ${link_options})
endif()

if (compile_options)
    target_compile_options(ucxx-compiler-options INTERFACE ${compile_options})
endif()

if (compile_definitions)
    target_compile_definitions(ucxx-compiler-options INTERFACE
        ${compile_definitions})
endif()
