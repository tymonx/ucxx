# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include(ExternalProject)

find_package(Threads REQUIRED)

set(args
    -DCMAKE_POSITION_INDEPENDENT_CODE=ON
)

if (CMAKE_C_COMPILER)
    list(APPEND args -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER})
endif()

if (CMAKE_CXX_COMPILER)
    list(APPEND args -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER})
endif()

if (CMAKE_CXX_COMPILER_ID MATCHES Clang OR CMAKE_CXX_COMPILER_ID MATCHES GNU)
    list(APPEND args -DCMAKE_CXX_FLAGS=-std=c++17)
endif()

ExternalProject_Add(googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG release-1.8.1
    GIT_PROGRESS TRUE
    PREFIX "${CMAKE_BINARY_DIR}/googletest"
    CMAKE_ARGS ${args}
    PATCH_COMMAND ""
    UPDATE_COMMAND ""
    INSTALL_COMMAND ""
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON
)

ExternalProject_Get_Property(googletest source_dir)
ExternalProject_Get_Property(googletest binary_dir)

add_library(gtest STATIC IMPORTED GLOBAL)
add_library(gmock STATIC IMPORTED GLOBAL)
add_library(gtest_main STATIC IMPORTED GLOBAL)
add_library(gmock_main STATIC IMPORTED GLOBAL)

file(MAKE_DIRECTORY "${source_dir}/googletest/include")
file(MAKE_DIRECTORY "${source_dir}/googlemock/include")

set_target_properties(gtest PROPERTIES
    IMPORTED_LOCATION "${binary_dir}/googlemock/gtest/libgtest.a"
    IMPORTED_LINK_INTERFACE_LIBRARIES "${CMAKE_THREAD_LIBS_INIT}"
    INTERFACE_INCLUDE_DIRECTORIES "${source_dir}/googletest/include"
)

set_target_properties(gmock PROPERTIES
    IMPORTED_LOCATION "${binary_dir}/googlemock/libgmock.a"
    IMPORTED_LINK_INTERFACE_LIBRARIES "${CMAKE_THREAD_LIBS_INIT}"
    INTERFACE_INCLUDE_DIRECTORIES "${source_dir}/googlemock/include"
)

set_target_properties(gtest_main PROPERTIES
    IMPORTED_LOCATION "${binary_dir}/googlemock/gtest/libgtest_main.a"
    IMPORTED_LINK_INTERFACE_LIBRARIES "${CMAKE_THREAD_LIBS_INIT}"
    INTERFACE_INCLUDE_DIRECTORIES "${source_dir}/googletest/include"
)

set_target_properties(gmock_main PROPERTIES
    IMPORTED_LOCATION "${binary_dir}/googlemock/libgmock_main.a"
    IMPORTED_LINK_INTERFACE_LIBRARIES "${CMAKE_THREAD_LIBS_INIT}"
    INTERFACE_INCLUDE_DIRECTORIES "${source_dir}/googlemock/include"
)

add_dependencies(gtest googletest)
add_dependencies(gmock googletest)
add_dependencies(gtest_main googletest)
add_dependencies(gmock_main googletest)
