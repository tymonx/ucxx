# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (COMMAND _ucxx_find_lcov)
    return()
endif()

function(_ucxx_find_lcov)
    set(LCOV_FOUND FALSE)

    set(LCOV_COMPONENTS
        genhtml
    )

    find_program(LCOV_EXECUTABLE
        NAMES lcov
        DOC "A graphical GCOV front-end"
    )

    mark_as_advanced(LCOV_EXECUTABLE)

    if (LCOV_EXECUTABLE)
        set(LCOV_FOUND TRUE)

        if (NOT TARGET LCOV::lcov)
            add_executable(LCOV::lcov IMPORTED GLOBAL)
        endif()

        set_target_properties(LCOV::lcov PROPERTIES
            IMPORTED_LOCATION "${LCOV_EXECUTABLE}"
        )

        execute_process(
            COMMAND "${LCOV_EXECUTABLE}" --version
            OUTPUT_VARIABLE LCOV_VERSION
            OUTPUT_STRIP_TRAILING_WHITESPACE
            RESULT_VARIABLE result
        )

        set(LCOV_VERSION_REGEX "[0-9]+(\.[0-9]+)?(\.[0-9]+)?")

        if (result EQUAL 0)
            string(REGEX REPLACE "^.* (${LCOV_VERSION_REGEX}).*$" "\\1"
                LCOV_VERSION "${LCOV_VERSION}"
            )

            if (NOT LCOV_VERSION MATCHES "^${LCOV_VERSION_REGEX}$")
                message(WARNING "Unable to determine lcov version")
            endif()
        else()
            message(WARNING "Cannot get lcov version")
        endif()
    endif()

    set(LCOV_FOUND "${LCOV_FOUND}" PARENT_SCOPE)
    set(LCOV_VERSION "${LCOV_VERSION}" PARENT_SCOPE)

    foreach (component IN LISTS LCOV_FIND_COMPONENTS)
        if (component IN_LIST LCOV_COMPONENTS)
            set(LCOV_${component}_FOUND FALSE)

            find_program(LCOV_${component}_EXECUTABLE
                NAMES ${component}
                DOC "The ${component} tool"
            )

            mark_as_advanced(LCOV_${component}_EXECUTABLE)

            if (LCOV_${component}_EXECUTABLE)
                set(LCOV_${component}_FOUND TRUE)

                if (NOT TARGET LCOV::${component})
                    add_executable(LCOV::${component} IMPORTED GLOBAL)
                endif()

                set_target_properties(LCOV::${component} PROPERTIES
                    IMPORTED_LOCATION "${LCOV_${component}_EXECUTABLE}"
                )
            endif()

            set(LCOV_${component}_FOUND "${LCOV_${component}_FOUND}"
                PARENT_SCOPE
            )
        else()
            message(WARNING "${component} is not a valid LCOV component")
        endif()
    endforeach()

    include(FindPackageHandleStandardArgs)

    find_package_handle_standard_args(LCOV
        REQUIRED_VARS LCOV_EXECUTABLE
        VERSION_VAR LCOV_VERSION
        HANDLE_COMPONENTS
    )
endfunction()

_ucxx_find_lcov()
