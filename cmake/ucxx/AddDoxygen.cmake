# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (COMMAND _ucxx_add_doxygen)
    return()
endif()

function(_ucxx_add_doxygen)
    find_package(Doxygen)

    if (DOXYGEN_FOUND)
        set(include_headers "")

        set(include_directories
            "${CMAKE_SOURCE_DIR}/include"
        )

        set(file_extensions
            h
            H
            hh
            HH
            hpp
            HPP
            h++
            H++
            hxx
            HXX
        )

        foreach (include_directory IN LISTS include_directories)
            set(file_expressions "")

            foreach (file_extension IN LISTS file_extensions)
                list(APPEND file_expressions
                    "${include_directory}/*.${file_extension}")
            endforeach()

            file(GLOB_RECURSE headers ${file_expressions})

            list(APPEND include_headers ${headers})
        endforeach()

        list(REMOVE_DUPLICATES include_headers)

        set(DOXYGEN_RECURSIVE NO)
        set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/doc")

        doxygen_add_docs(doxygen
            ${include_headers}
            WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
            COMMENT "Generating a source code documentation"
        )

        if (NOT TARGET doc)
            add_custom_target(doc)
            add_dependencies(doc doxygen)
        endif()
    else()
        add_custom_target(doxygen
            "${CMAKE_COMMAND}" -E echo "Doxygen was not found"
        )

        if (NOT TARGET doc)
            add_custom_target(doc)
        endif()
    endif()
endfunction()

_ucxx_add_doxygen()
