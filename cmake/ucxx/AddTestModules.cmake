# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (COMMAND ucxx_test_modules)
    return()
endif()

function(ucxx_test_modules)
    foreach (name IN LISTS ARGV)
        add_executable(test_${name}
            test_${name}.cpp
        )

        target_link_libraries(test_${name} PRIVATE
            ucxx-compiler-options
            ucxx
            gtest
            gtest_main
            gmock
        )

        if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
            target_compile_options(test_${name} PRIVATE
                -Wno-global-constructors
                -Wno-zero-as-null-pointer-constant
            )
        endif()

        add_test(test_${name}_build
            "${CMAKE_COMMAND}"
            --build "${CMAKE_BINARY_DIR}"
            --config $<CONFIG>
            --target test_${name}
        )

        add_test(NAME test_${name} COMMAND test_${name})

        if (UCXX_COVERAGE)
            set_tests_properties(test_${name}_build PROPERTIES FIXTURES_REQUIRED
                test_build_fixture)

            set_tests_properties(test_${name}_build PROPERTIES FIXTURES_SETUP
                test_coverage_initial_fixture)

            set_tests_properties(test_${name} PROPERTIES FIXTURES_REQUIRED
                test_fixture)

            set_tests_properties(test_${name} PROPERTIES FIXTURES_SETUP
                test_coverage_capture_fixture)
        else()
            set_tests_properties(test_${name}_build PROPERTIES FIXTURES_REQUIRED
                test_cmake_regenerate_fixture)

            set_tests_properties(test_${name}_build PROPERTIES FIXTURES_SETUP
                test_${name}_fixture)

            set_tests_properties(test_${name} PROPERTIES FIXTURES_REQUIRED
                test_${name}_fixture)
        endif()
    endforeach()
endfunction()

function(_ucxx_add_test_modules)
    add_test(test_cmake_regenerate
        "${CMAKE_COMMAND}"
        -S "${CMAKE_SOURCE_DIR}"
        -B "${CMAKE_BINARY_DIR}"
    )

    set_tests_properties(test_cmake_regenerate PROPERTIES FIXTURES_SETUP
        test_cmake_regenerate_fixture)

    if (UCXX_COVERAGE)
        list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

        find_package(LCOV REQUIRED COMPONENTS genhtml)

        set(lcov_gcov_tool gcov)

        file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/coverage")

        add_test(NAME test_coverage_zeroing COMMAND LCOV::lcov
            --zerocounters
            --base-directory "${CMAKE_SOURCE_DIR}"
            --directory "${CMAKE_BINARY_DIR}"
        )

        add_test(NAME test_coverage_initial COMMAND LCOV::lcov
            --capture
            --initial
            --base-directory "${CMAKE_SOURCE_DIR}"
            --directory "${CMAKE_BINARY_DIR}"
            --output-file "${CMAKE_BINARY_DIR}/coverage_base.info"
        )

        if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
            set(lcov_gcov_tool "${CMAKE_BINARY_DIR}/llvm-gcov")

            file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/tmp")

            file(WRITE "${CMAKE_BINARY_DIR}/tmp/llvm-gcov"
                "#!/usr/bin/env bash\n")

            file(APPEND "${CMAKE_BINARY_DIR}/tmp/llvm-gcov"
                "exec llvm-cov gcov \"$@\"\n")

            file(COPY "${CMAKE_BINARY_DIR}/tmp/llvm-gcov"
                DESTINATION "${CMAKE_BINARY_DIR}"
                FILE_PERMISSIONS
                OWNER_READ
                OWNER_WRITE
                OWNER_EXECUTE
                GROUP_READ
                GROUP_EXECUTE
                WORLD_READ
                WORLD_EXECUTE
            )
        endif()

        add_test(NAME test_coverage_capture COMMAND LCOV::lcov
            --capture
            --gcov-tool ${lcov_gcov_tool}
            --base-directory "${CMAKE_SOURCE_DIR}"
            --directory "${CMAKE_BINARY_DIR}"
            --output-file "${CMAKE_BINARY_DIR}/coverage_test.info"
        )

        add_test(NAME test_coverage_combine COMMAND LCOV::lcov
            --add-tracefile "${CMAKE_BINARY_DIR}/coverage_base.info"
            --add-tracefile "${CMAKE_BINARY_DIR}/coverage_test.info"
            --output-file "${CMAKE_BINARY_DIR}/coverage_total.info"
        )

        add_test(NAME test_coverage_filter COMMAND LCOV::lcov
            --remove "${CMAKE_BINARY_DIR}/coverage_total.info"
            "\*/gtest/\*"
            "\*/tests/\*"
            "/usr\*"
            --output-file "${CMAKE_BINARY_DIR}/coverage.info"
        )

        add_test(NAME test_coverage_generate COMMAND LCOV::genhtml
            --prefix "${CMAKE_SOURCE_DIR}"
            "${CMAKE_BINARY_DIR}/coverage.info"
            --legend
            --title "${CMAKE_PROJECT_NAME}"
            --demangle-cpp
            --output-directory "${CMAKE_BINARY_DIR}/coverage"
        )

        set_tests_properties(test_coverage_zeroing PROPERTIES FIXTURES_REQUIRED
            test_cmake_regenerate_fixture)

        set_tests_properties(test_coverage_zeroing PROPERTIES FIXTURES_SETUP
            test_build_fixture)

        set_tests_properties(test_coverage_initial PROPERTIES FIXTURES_REQUIRED
            test_coverage_initial_fixture)

        set_tests_properties(test_coverage_initial PROPERTIES FIXTURES_SETUP
            test_fixture)

        set_tests_properties(test_coverage_capture PROPERTIES FIXTURES_REQUIRED
            test_coverage_capture_fixture)

        set_tests_properties(test_coverage_capture PROPERTIES FIXTURES_SETUP
            test_coverage_combine_fixture)

        set_tests_properties(test_coverage_combine PROPERTIES FIXTURES_REQUIRED
            test_coverage_combine_fixture)

        set_tests_properties(test_coverage_combine PROPERTIES FIXTURES_SETUP
            test_coverage_filter_fixture)

        set_tests_properties(test_coverage_filter PROPERTIES FIXTURES_REQUIRED
            test_coverage_filter_fixture)

        set_tests_properties(test_coverage_filter PROPERTIES FIXTURES_SETUP
            test_coverage_generate_fixture)

        set_tests_properties(test_coverage_generate PROPERTIES FIXTURES_REQUIRED
            test_coverage_generate_fixture)

        add_custom_command(OUTPUT "${CMAKE_BINARY_DIR}/coverage.info"
            COMMAND "${CMAKE_COMMAND}" ARGS
                --build "${CMAKE_BINARY_DIR}"
                --config $<CONFIG>
                --target test
            COMMENT "Generating a code coverage file for a summary"
        )

        add_custom_target(coverage-summary
            LCOV::lcov --list "${CMAKE_BINARY_DIR}/coverage.info"
            DEPENDS "${CMAKE_BINARY_DIR}/coverage.info"
            COMMENT "Showing a summary code coverage information"
        )
    else()
        add_custom_target(coverage-summary
            "${CMAKE_COMMAND}" -E echo "Code coverage was not enabled"
        )
    endif()
endfunction()

_ucxx_add_test_modules()
