# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (NOT CMAKE_CXX_COMPILER_ID MATCHES Clang)
    return()
endif()

if (NOT TARGET ucxx-compiler-options)
    add_library(ucxx-compiler-options INTERFACE)
endif()

set(link_options "")
set(compile_options "")
set(compile_definitions "")

list(APPEND compile_options -std=c++17)

if (NOT UCXX_EXCEPTIONS)
    list(APPEND compile_options -fno-exceptions)
endif()

if (NOT UCXX_RTTI)
    list(APPEND compile_options -fno-rtti)
endif()

if (UCXX_LTO)
    list(APPEND compile_options -flto)
endif()

if (UCXX_COVERAGE)
    list(APPEND link_options
        --coverage
    )

    list(APPEND compile_options
        -fprofile-arcs
        -ftest-coverage
        -fno-inline
        -fno-inline-small-functions
        -fno-default-inline
    )
endif()

list(APPEND compile_options
    -pedantic
    -fstrict-aliasing
    -Weverything
    -Wno-padded
    -Wno-covered-switch-default
    -Wno-c++98-compat
    -Wno-c++98-compat-pedantic
)

if (UCXX_WARNINGS_INTO_ERRORS)
    list(APPEND compile_options -Werror)
endif()

if (CMAKE_BUILD_TYPE MATCHES "Release" OR NOT CMAKE_BUILD_TYPE)
    list(APPEND compile_options
        -O3
        -fdata-sections
        -ffunction-sections
    )

    list(APPEND compile_definitions NDEBUG)

    if (NOT CMAKE_SYSTEM_NAME MATCHES "Generic")
        list(APPEND link_options -fstack-protector-all)

        if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.5)
            list(APPEND link_options -fstack-protector-strong)
        endif()
    endif()

    list(APPEND link_options
        -Wl,--gc-sections
        -Wl,--strip-all
        -Wl,-z,noexecstack
        -Wl,-z,relro
        -Wl,-z,now
    )
elseif (CMAKE_BUILD_TYPE MATCHES "MinSizeRel")
    list(APPEND compile_options
        -Os
        -fdata-sections
        -ffunction-sections
    )

    list(APPEND compile_definitions NDEBUG)
    list(APPEND link_options -Wl,--gc-sections)
elseif (CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
    list(APPEND compile_options
        -O3
        -g3
        -ggdb
        -fdata-sections
        -ffunction-sections
    )

    if (NOT CMAKE_SYSTEM_NAME MATCHES "Generic")
        list(APPEND link_options -fstack-protector-all)

        if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.5)
            list(APPEND link_options -fstack-protector-strong)
        endif()
    endif()

    list(APPEND link_options
        -Wl,--gc-sections
        -Wl,-z,noexecstack
        -Wl,-z,relro
        -Wl,-z,now
    )
elseif (CMAKE_BUILD_TYPE MATCHES "Debug")
    list(APPEND compile_options
        -O0
        -g3
        -ggdb
    )
endif()

if (link_options)
    target_link_options(ucxx-compiler-options INTERFACE ${link_options})
endif()

if (compile_options)
    target_compile_options(ucxx-compiler-options INTERFACE ${compile_options})
endif()

if (compile_definitions)
    target_compile_definitions(ucxx-compiler-options INTERFACE
        ${compile_definitions})
endif()
